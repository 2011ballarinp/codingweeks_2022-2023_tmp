# Feature 2: A utility to access the corpus data.

In order to be able to manipulate correctly the information contained in the corpus, it is necessary to program a set of tools allowing to retrieve the useful information. To do this, we will create a corpusutils module inside the `datavisualization` project.  We remind you that the correct way to do this is to add an empty `__init.py__` file to the root of the `datavisualization` directory. If you are not familiar with this notion of modules and packages, we invite you to consult the tutorial at the beginning [here](https://github.com/hudelotc/CentraleSupelec_CodingWeeks_2020/blob/main/modulespackagespython.md) or this [documentation](https://chrisyeh96.github.io/2017/08/08/definitive-guide-python-imports.html).

We will build this corpusutils module by iterations.


## Iteration 1: a brat annotation reader.

To be able to retrieve the information contained in the annotations, it is necessary to be able to parse the `.ann` files. Your first task is to create a reader for `.ann' files. This reader will be implemented in a `read_and_load_ann_annotation(filename)` function which, given the `filename` path of an annotation file, opens that file and returns useful information in the form of a python `dict` dictionary. 

To understand these annotations, let's look for example at the tweet with ID `143048389142134785` whose textual content is as follows:

```
Tonight is the #missfrance election and I'm going to make a point of NOT watching.
```
The corresponding annotation given in the file `143048389142134785.ann` is : 

```
R1	Negates Arg1:T3 Arg2:T2	
R2	isNegativeOpinionOn Arg1:T2 Arg2:T1	
T2	Subjectiveme_positive 63 70	plaisir
T3	Negator 77 80	PAS
T1	Topic 16 39	élection de #missfrance
```

**How to read and understand this file

This annotation indicates that the subject (`Topic`) of this text is `election of #missfrance`. This information is present in the text of the tweet from the `16` character to the `39` character. The tweet also contains a negation indicator `NOT` which is found between the characters `77` and `80` in the text and a *positivity* indicator, the word `pleasure` which is found between the characters `63` and `70` in the text. The annotation then gives the information that in the tweet, a negative information is given (line R2 of the annotation reads as `pleasure` is a negative opinion of `election of #missfrance`) and that we should consider `No pleasure` in this tweet and not `pleasure` (line R1 of the annotation).

This example shows the richness of annotations in the [brat] format (http://brat.nlplab.org/standoff.html). In this project, we will not use all this information, at least not for the MVP. 

For a given tweet, we want to have:

+ The set of topics it deals with (a `Topic` entity)
+ For each topic, whether the opinion is positive or negative about it.
+ The set of keywords considered as positive in the tweet.
+ The set of keywords considered as negative in the tweet.


For example, for the above annotation, we would like to retrieve the useful information in this form (python dictionary).

```PYTHON
{ 'topics': [{ 'name': 'election of #missfrance', 'opinion': 'negative'}], 'negative_keywords': ['no pleasure'], 'positive_keywords': ['pleasure']}
```

Here is another example:

```
T1	Topic 9 18	Languedoc
T3	Subjectiveme_positive 30 35	jolie
R2	isPositiveOpinionOn Arg1:T3 Arg2:T1	
T5	Topic 71 89	Nord-Pas-De-Calais
T2	Negator 49 52	pas
T4	Subjectiveme_positive 44 48	aime
R1	Negates Arg1:T2 Arg2:T4	
R3	isNegativeOpinionOn Arg1:T4 Arg2:T5
```

And the associated dictionary:

```PYTHON
{ 'topics': [{ 'name': 'Languedoc', 'opinion': 'positive'}, { 'name': 'Nord-Pas-De-Calais', 'opinicon': 'negative'}], 'negative_keywords': ['dislike'], 'positive_keywords': ['pretty', 'like']}

```

Of course, we lose some information by doing this, but it will be enough for the implementation of our MVP.


To write this annotation reader, we will use the [**TDD (Test Driven Development)**](https://fr.wikipedia.org/wiki/Test_driven_development) development approach which consists in specifying the expected behavior via a test before actually implementing it (this is what we have typically just done).


The principle is therefore to first write the test and then the simplest possible code that allows the test to pass and thus satisfy the specified behavior. The code can then be improved. The idea is to focus on the functionality rather than on the code.

We will first work step by step and then you will become more and more autonomous in this approach. 


1. **Acceptance criteria**

 One of the first tasks to be done here is to look for and list all the criteria that will allow to correctly answer the needs that the *Parser annotation* functionality is supposed to cover. These criteria are **acceptance criteria**. Here, it's very simple, the acceptance criterion of *Parser an annotation ann* is to **have in output a dictionary containing the useful information**.

 2. **Test Driven Development** (TDD)

 TDD (Test Driven Development)** is a test driven development and therefore the first line of your program must be in a test file. In our case, we will use the [`pytest`] module (https://docs.pytest.org/en/latest/) which must be added to your project. The principle of TDD is based on 3 complementary steps.
 
   + First step (**<span style='color:red'>RED</span>**): Write a first failed test.
   + Second step (**<span style='color:green'>GREEN</span>**): Write the simplest code that allows passing the code.
   + Third step (**REFACTOR**): Improve the source code.


We will therefore apply this method to the parsing functionality of the ann file


#### **<span style='color:red'> STEP RED</span>**

Our first test will consist of testing that the parsing returns a well-founded dictionary when reading an ann file.

```PYTHON
from datavisualization.corpusutils import read_and_load_annotation
from pytest import *


def test_read_and_load_annotation():
    # Given
    filename = "143048389142134785.ann"
    # When
    annotations = read_and_load_annotation(filename)
    #Then
    assert annotations = {'topics' : [{ 'name' : "élection de #missfrance", 'opinion':'negative' }], 'negative_keywords': ["pas plaisir"], 'positive_keywords':["plaisir"]}
    # Given
    filename1 = "143059118180139008.ann"
    # When
    annotations1 = read_and_load_annotation(filename1)
    #Then
    assert annotations1 = {'topics' : [{ 'name' : "Languedoc", 'opinion':'positive' },{ 'name' : "Nord-Pas-De-Calais", 'opinicon':'negative' } ], 'negative_keywords': ["pas aime"], 'positive_keywords':["jolie", "aime"]}



```

Copy this code into a `test_read_and_load_annotation.py` file.
This test must fail, since in the current state of the project, the code for `read_and_load_annotation` does not exist and we therefore have an error when executing the code.

#### **<span style='color:green'> STEP GREEN</span>**

We will now write the code that allows this test to pass as quickly as possible.

All you need to do is:

 + Create and complete a `read_and_load_annotation(filename)` function in the `corpusutils.py` file such that the previous test passes.
 
Your test should turn green with this step **<span style='color:green'>STEP GREEN</span>**
 

#### **<span style='color:black'> REFACTOR STEP</span>**
   
The last step consists of a [refactoring](https://refactoring.com/) step, to be implemented if necessary.

[Refactoring](https://en.wikipedia.org/wiki/Code_refactoring) is a programming principle that consists of changing the internal structure of software without changing its observable behavior. This is a step that must always be performed when the various tests are green and is not mandatory. Above all, it should make it possible to improve the **quality of the code**, for example by improving:
 
 + **design**: division into functions, modules or classes to make your code as simple as possible.
 + **the readability of the code**: here you have to take the time to apply the principles of [clean code](https://cleancoders.com/cart) introduced by Robert C. Martin in the book of the same name and one of whose principles is that of the Boy Scouts (*“The Boy Scout Rule”*): *“Always leave a place better than you found it”*.
 
In our case, one of the first principles is to check the correct naming (variables, functions, packages, classes and co.) and the presence of comments in our code.
 
 You will find [here](https://github.com/zedr/clean-code-python#objects-and-data-structures) some principles of clean code transposed to the python language. Take the time to quickly read this site and apply these different principles to the code you are about to write.
 
In this step **<span style='color:black'> REFACTOR STEP</span>**, you can also work on optimizing the performance of the program if this is really necessary.
 
 
#### **WARNING**

1. **After this step, don't forget to run the tests again to check that the behavior of your code has not changed and that everything is still GREEN!**

2. Here we have just finished the realization of the step *a brat annotation reader* and it is therefore necessary to **commit this change in your version manager with an explicit commit message repeating the objective of the step **. Also remember to update your remote repository.
 
 
In the following, we advise you to use the TDD approach but it is not mandatory. However, you should always take care **to test your programs**.
 
 
 

## Iteration 2: Associate a tweet with an annotation



For a given tweet in the corpus, it is necessary to check that an annotation is available in the corpus and it is necessary to be able to retrieve and store this information in a data structure which will be able to facilitate their processing. We will use here a python dictionary `dict` which will therefore contain the following keys:

+ `"id"`: the id of the tweet
+ `"text"`: the text of the tweet
+ `"annotation"`: the annotation of the tweet as obtained in the previous step.

Write and test the `load_tweet_with_annotation(id)` function which given the `id` of a tweet builds such a dictionary. We will take great care to test this function and to manage the case where a tweet has no annotation. It must also be done with the TDD approach.

#### <span style="color: #26B260">At this stage of the project, you have reached MILESTONE 3: Write code in a TDD approach </span>



## About the code coverage of your tests

A code coverage by the tests (code coverage) allows us to know the percentage of our code which is tested and therefore it allows to have an idea of ​​what remains of shadow in our project.

As a general rule, we consider that a code coverage greater than 80% is a sign of a well-tested project and it will then be easier to add new features to this project.

To know the rate of coverage by the tests of your project, you can use the python libraries [`coverage`] and [`pytest_cov`] which must therefore be installed

`pip3 install coverage` ou `pip install coverage`

`pip3 install pytest-cov` ou `pip install pytest-cov`


You must then go to the directory of your project and run the following command:

`pytest --cov=datavisualization --cov-report html test_*.py`

This command allows you to test the files contained in the `datavisualization` folder. It creates a report in html and places it in the directory `htmlcov` and uses the tests which are in this directory and which are of the form `test_[characters].py`.
Opening the `index.html` file in the `htmlcov` directory allows you to view a coverage test result which should be good since we used the TDD approach. A click on each of the files also allows you to have a report specific to each file as illustrated in the images below (and which shows the result for another project).

![testcoverage](./Images/testcoverage.png)



![testcoverage](./Images/testcoveragebis.png)




#### <span style="color: #26B260">At this stage of the project, you have reached MILESTONE 5: a first coverage of my project by tests </span>



## About versioning

<span style='color:blue'> For the rest of the project, you are asked to:</span>

+ <span style='color:blue'>Make a commit as soon as a feature or sub-feature is finished.</span>
+ <span style='color:blue'>Tagger at the end of each day your last commit </span>
+ <span style='color:blue'>Do a code review within the team for each feature.</span>
+ <span style='color:blue'>To put the stable code on the `master` branch.</span>
+ <span style='color:blue'>Push the code to your remote repository on GitLab.</span>
+ <span style='color:blue'>Do a code coverage test at the end of each day and push the result to your remote repository on GitLab.</span>



you can now proceed to [**Feature 3**: Get started with `pandas`.](./S1_pandas.md)
